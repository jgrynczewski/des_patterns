class Journal:
    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f"{self.count}: {text}")

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return "\n".join(self.entries)

    # łamiemy zasadę SRP
    def save_to_file(self, filename):
        file = open(filename, 'w')
        file.write(str(self))
        file.close()

    def load_from_file(self):
        pass

    def load_from_web(self):
        pass


j = Journal()
j.add_entry("Wstałem")
j.add_entry("Zjadłem śniadanie")
print("Moje wpisy:")
print(j)
j.save_to_file("journal.txt") # Tutja łamiemy zasadę SRP


class PersistanceManager:

    @staticmethod
    def save_to_file(journal, filename):
        file = open(filename, 'w')
        file.write(str(journal))
        file.close()

j = Journal()
j.add_entry("Położyłem się")
j.add_entry("Zasnąłem")

file = "journal2.txt"
PersistanceManager().save_to_file(j, file)

with open(file, 'r') as fh:
    print(fh.read())

# Antywzorzec łamiący SRP - God Object