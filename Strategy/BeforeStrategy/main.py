from before_strategy.shipper import Shipper
from before_strategy.order import Order
from before_strategy.shipping_cost import ShippingCost

# Test Federal Express shipper
order = Order(Shipper.FEDEX)
cost_calculator = ShippingCost()
cost = cost_calculator.shipping_cost(order)
assert cost == 3.0

# Test UPS shipper
order = Order(Shipper.UPS)
cost_calculator = ShippingCost()
cost = cost_calculator.shipping_cost(order)
assert cost == 4.0

# Test postal shipper
order = Order(Shipper.POSTAL)
cost_calculator = ShippingCost()
cost = cost_calculator.shipping_cost(order)
assert cost == 5.0

print("Tests passed")