from enum import Enum

class Relationship(Enum):
    PARENT = 0
    CHILD = 1
    SILBLING = 2

class Person:
    def __init__(self, name):
        self.name = name

class Relationships:
    def __init__(self):
        self.relations = []

    def add_parent_and_child(self, parent, child):
        self.relations.append(
            (parent, Relationship.PARENT, child)
        )
        self.relations.append(
            (child, Relationship.CHILD, parent)
        )

# łamiemy regułę DIP

class Research:
    def __init__(self, relationships):
        relations = relationships.relations
        for r in relations:
            if r[0].name == "Jan" and r[1] == Relationship.PARENT:
                print(f"Jan ma dziecko o imieniu {r[2].name}")

p1 = Person("Jan")
p2 = Person("Kuba")
p3 = Person("Karol")

relationships = Relationships()
relationships.add_parent_and_child(p1, p2)
relationships.add_parent_and_child(p1, p3)

Research(relationships)

from abc import abstractmethod

class RelationshipsBrowser:
    @abstractmethod
    def find_all_children_of(self, name): pass


class Relationships(RelationshipsBrowser):
    def __init__(self):
        self.relations = []

    def add_parent_and_child(self, parent, child):
        self.relations.append(
            (parent, Relationship.PARENT, child)
        )
        self.relations.append(
            (child, Relationship.CHILD, parent)
        )

    def find_all_children_of(self, name):
        for r in self.relations:
            if r[0].name == "Jan" and r[1] == Relationship.PARENT:
                yield r[2].name

class Research:
    def __init__(self, browser):
        for p in browser.find_all_children_of("Jan"):
            print(f"Jan ma dziecko o imieniu {p}")

p1 = Person("Jan")
p2 = Person("Kuba")
p3 = Person("Karol")

relationships = Relationships()
relationships.add_parent_and_child(p1, p2)
relationships.add_parent_and_child(p1, p3)

Research(relationships)