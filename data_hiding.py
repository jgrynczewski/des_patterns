# Modyfikatory dostępu _ - chroniony, __ - prywatny
# atrybuty publiczne, prywatne, chronione

# Wersja 1
class Dog:
    def __init__(self, name, age, rasa):
        self.name = name # atrybuty publiczne
        self.__age = age # atrybuty prywatne
        self._rasa = rasa # atrybut chroniny

    def rename(self, name2):
        self.name = name2

    def make_younger(self, years):
        self.__age -= years
        print(self.__age)

    #getter
    def get_age(self):
        return self.__age

    #setter
    def set_age(self, new_age):
        if new_age > 50:
            print("Psy tyle nie żyją")
        else:
            self.__age = new_age

pies1 = Dog("Azor", 5, 'buldog')
pies1.rename("Burek")

print(pies1.get_age())
pies1.set_age(52)
print(pies1.get_age())

print(pies1._rasa)

pies1.make_younger(2)

# Wersja 2
class Dog2:
    def __init__(self, name, age, rasa):
        self.name = name # atrybuty publiczne
        self.__age = age # atrybuty prywatne
        self._rasa = rasa # atrybut chroniny

    def rename(self, name2):
        self.name = name2

    def make_younger(self, years):
        self.__age -= years
        print(self.__age)

    #getter (akcesor)
    @property
    def age(self):
        return self.__age

    #setter (mutator)
    @age.setter
    def age(self, new_age):
        if new_age > 50:
            print("Psy tyle nie żyją")
        else:
            self.__age = new_age

pies1 = Dog2("Azor", 5, 'buldog')
pies1.rename("Burek")

print(pies1.age)
pies1.age = 52
print(pies1.age)