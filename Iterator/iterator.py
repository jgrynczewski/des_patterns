from collections.abc import Iterator, Iterable

class WordCollectionIterator(Iterator):

    def __init__(self, collection, reverse=False):
        self._collection = collection
        self._reverse = reverse
        self._position = -1 if self._reverse else 0

    def __next__(self):
        try:
            value = self._collection[self._position]
            self._position += -1 if self._reverse else 1
            return value
        except IndexError:
            raise StopIteration

class WordCollection(Iterable):

    def __init__(self):
        self._collection = []

    def __iter__(self):
        return WordCollectionIterator(self._collection)

    def get_reverse_iterator(self):
        return WordCollectionIterator(self._collection, True)

    def add_item(self, item):
        self._collection.append(item)

collection = WordCollection()
collection.add_item("First")
collection.add_item("Second")
collection.add_item("Third")

print("Straight order")
for item in collection:
    print(item)

print()
print("Revesed order:")
for item in collection.get_reverse_iterator():
    print(item)