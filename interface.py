import abc

class MyABC(abc.ABC):
    """Abstract Base Class Definition"""

    @abc.abstractmethod
    def do_something(self, value):
        """Required method"""

    @property
    @abc.abstractmethod
    def some_property(self):
        """Required property"""


class MyClass(MyABC):
    """Implementation of MyABC"""

    def __init__(self, value=None):
        self._my_prop = value

    def do_something(self, value):
        """Implementation of abstract method"""
        return self._my_prop*2 - value

    @property
    def some_property(self):
        """Implementation of abstract property"""
        return self._my_prop


class BadClass(MyABC):
    pass

bad = BadClass()