from enum import Enum

class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3

class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Product:
    def __init__(self, name, color, size):
        self.name = name
        self.color = color
        self.size = size


p1 = Product("Bluzka", Color.GREEN, Size.MEDIUM)
p2 = Product("Bluzka 2", Color.BLUE, Size.LARGE)
p3 = Product("Spodnie", Color.GREEN, Size.LARGE)

products = [p1, p2 , p3]

class ProductFilter:
    def filter_by_color(self, products, color):
        for p in products:
            if p.color == color:
                yield p

    def filter_by_size(self, products, size):
        for p in products:
            if p.size == size:
                yield p

    def filter_by_color_and_by_size(self, products, color, size):
        for p in products:
            if p.color == color and p.size == size:
                yield p

pf = ProductFilter()
print("Zielone artykuły:")
for item in pf.filter_by_color(products, Color.GREEN):
    print(f" - {item.name}")
print("Artykuły o dużym rozmiarze:")
for item in pf.filter_by_size(products, Size.LARGE):
    print(f" - {item.name}")
print("Artykuły o dużym rozmiarze w kolorze zielonym:")
for item in pf.filter_by_color_and_by_size(products, Color.GREEN, Size.LARGE):
    print(f" - {item.name}")


# Wzorzec specyfikacja (jeden z enterprise design patterns)
class Specification:
    def is_satisfied(self, item):
        pass


class Filter:
    def filter(self, items, specification):
        pass


class ColorSpecification(Specification):

    def __init__(self, color):
        self.color = color

    def is_satisfied(self, item):
        return item.color == self.color

class SizeSpecification(Specification):

    def __init__(self, size):
        self.size = size

    def is_satisfied(self, item):
        return item.size == self.size


class ColorAndSizeSpecification(Specification):
    def __init__(self, spec1, spec2):
        self.spec1 = spec1
        self.spec2 = spec2

    def is_satisfied(self, item):
        return self.spec1.is_satisfied(item) and self.spec2.is_satisfied(item)


class NewFilter(Filter):
    def filter(self, items, spec):
        for item in items:
            if spec.is_satisfied(item):
                yield item


nf = NewFilter()
print("Artykuły o kolorze zielonym (nowy filtr)")
green = ColorSpecification(Color.GREEN)
for p in nf.filter(products, green):
    print(f"- {p.name}")
large = SizeSpecification(Size.LARGE)
print("Artykuły w rozmiarze duży (nowy filtr)")
for p in nf.filter(products, large):
    print(f"- {p.name}")
green_and_large = ColorAndSizeSpecification(green, large)
print("Artykuły w rozmiarze duży w kolorze zielonym (nowy filtr)")
for p in nf.filter(products, green_and_large):
    print(f"- {p.name}")
