from computer import Computer

my_computer = Computer()
my_computer.case = "Coolmaster N300"
my_computer.mainboard = "MSI 970"
my_computer.cpu = "Intel Core i7-4770"
my_computer.memory = "Corsair Vengeance 16GB"
my_computer.hard_disk = "Seagate 2TB"