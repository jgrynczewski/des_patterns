class Rectangle:
    def __init__(self, width, height):
        self._width = width
        self._height = height

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, new_width):
        self._width = new_width

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, new_height):
        self._height = new_height

    @property
    def area(self):
        return self._width * self._height

    def __str__(self):
        return f"Width: {self.width}, height: {self.height}"

def func(rc):
    w = rc.width
    rc.height = 10
    expected_area = 10 * w
    print(f"Oczekujemy pola o wartosci {expected_area}, dostajemy {rc.area}")

rc = Rectangle(2, 3)
func(rc)

class Square(Rectangle):
    def __init__(self, size):
        Rectangle.__init__(self, size, size)

    @Rectangle.width.setter
    def width(self, new_width):
        self._width = self._height = new_width

    @Rectangle.height.setter
    def height(self, new_height):
        self._width = self._height = new_height

sq = Square(5)
func(sq)