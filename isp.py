# Złamanie zasady ISP
class Machine:
    def print(self, document):
        raise NotImplementedError

    def scan(self, document):
        raise NotImplementedError

    def fax(self, document):
        raise NotImplementedError


class MultiFunctionPrinter(Machine):
    def print(self, document):
        "Jakiś kod"
        pass

    def scan(self, document):
        "Jakiś kod"
        pass

    def fax(self, document):
        "Jakiś kod"
        pass


class OldFashionedMachine(Machine):
    def print(self, document):
        "Jakiś kod"
        pass

    def scan(self, document):
        raise NotImplementedError("This printer cannoot scan")

    def fax(self, document):
        raise NotImplementedError("This printer canoot fax")

ofm = OldFashionedMachine()
ofm.fax()


from abc import abstractmethod

class Printer:
    @abstractmethod
    def print(self, document):
        pass

class Scanner:
    @abstractmethod
    def scan(self, document):
        pass

class Fax:
    @abstractmethod
    def fax(self, document):
        pass


class MyPrinter(Printer):
    def print(self, document):
        print(document)


class MyPhotoCopier(Printer, Scanner):
    def print(self, document):
        print(document)

    def scan(self, document):
        print(document)


class MultiFunctionDevice(Printer, Scanner, Fax):
    @abstractmethod
    def printer(self):
        pass
    @abstractmethod
    def scan(self, document):
        pass
    @abstractmethod
    def fax(self, document):
        pass


class MultiFunctionMachine(MultiFunctionDevice):
    def prinr(self, document):
        print(document)

    def scan(self, document):
        pass

    def fax(self, document):
        pass

